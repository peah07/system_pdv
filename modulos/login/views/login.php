<!DOCTYPE html>
<html >
<head>

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Login</title>
    <link href="<?php echo base_url(); ?>assets/pdv/img/favicon.ico" rel="shortcut icon" type="img/vnd.microsoft.icon">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/animate/animate.css">
    <!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/select2/select2.min.css">
    <!--===============================================================================================-->  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/TelaLogin/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css" type="text/css" />
    <!--===============================================================================================
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery-1.7.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>-->
    <script type="text/javascript">
        $(document).ready(function(){
           $('.loginform button').hover(function(){
              $(this).stop().switchClass('default','hover');
          },function(){
              $(this).stop().switchClass('hover','default');
          });

           $('#login').submit(function(){
              var u = jQuery(this).find('#username');
              if(u.val() == '') {
                 jQuery('.loginerror').slideDown();
                 u.focus();
                 return false;	
             }
         });

           $('#username').keypress(function(){
              jQuery('.loginerror').slideUp();
          });
       });
   </script>
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<meta charset="UTF-8"></head>

<body class="login">
<!--
<div class="loginbox radius3">
	<div class="loginboxinner radius3">
    	<div class="loginheader">
    		<h1 class="bebas" style="text-align:center">PIZZARIA ARTE E SABOR</h1>
        	
    	</div>loginheader
        
        <div class="loginform">
        	<div class="loginerror"><p><?php if($msg){ echo 'Usuario ou senha incorreto.'; } ?></p></div>
        	<?php echo form_open('login/valida');?>
            	<p>
                	<label for="username" class="bebas">Usuario</label>
                    <input type="text" id="username" name="usuario" class="radius2" />
                </p>
                <p>
                	<label for="password" class="bebas">Senha</label>
                    <input type="password" id="password" name="senha" class="radius2" />
                </p>
                <p>
                	<button class="radius3 bebas">Acessar</button>
                </p>
            <?php echo form_close();?>
        </div><!loginform-->
        <!-- </div>loginboxinner-->
        <!--</div>loginbox-->
        <div class="limiter">
            <div class="container-login100">
                <div class="loginerror"><p><?php if($msg){ echo 'Usuario ou senha incorreto.'; } ?></p></div>
                <div class=" loginform wrap-login100  p-b-160 p-t-50">
                    <div class="login100-form-title" style="background-image: url(<?php echo base_url(); ?>assets/TelaLogin/images/slide1.png);">
                        <span class="login100-form-title-1">
                            Bem vindo
                        </span>
                    </div>
                    <?php
                    $attributes = array('class' => 'login100-form validate-form', 'id' => 'myform');
                    echo form_open('login/valida',$attributes);

                    ?>
                    <!--<form class="login100-form validate-form">-->
                

                        <div class="wrap-input100 rs1 validate-input" data-validate = "Username is required">
                            <input class="input100" type="text" id="username" name="usuario">
                            <span class="label-input100">Usuario</span>
                        </div>


                        <div class="wrap-input100 rs2 validate-input" data-validate="Password is required">
                            <input class="input100" type="password" id="password" name="senha">
                            <span class="label-input100">Senha</span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="my-2 btn btn-primary btn-lg btn-block">
                                Entrar
                            </button>
                        </div>

                        
                        <!-- </form>-->
                        <?php echo form_close();?>
                    </div>
                </div>
            </div>



            
            <!--===============================================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/animsition/js/animsition.min.js"></script>
            <!--===============================================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/bootstrap/js/popper.js"></script>
            
            <!--=============Bootstrap====================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/jquery/jquery-3.2.1.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/bootstrap/js/bootstrap.min.js"></script>
            <!--==============Bootstrap===================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/select2/select2.min.js"></script>
            <!--===============================================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/daterangepicker/moment.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/daterangepicker/daterangepicker.js"></script>
            <!--===============================================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/vendor/countdowntime/countdowntime.js"></script>
            <!--===============================================================================================-->
            <script src="<?php echo base_url(); ?>assets/TelaLogin/js/main.js"></script>
        </body>
        </html>
